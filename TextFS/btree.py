nodes = []
	
class Node():
	def __init__(self,_t,_leaf):
		self.keys = []		# list of keys
		self.t = _t 		 		# max no. of keys that can be stored
		self.C = []			# list of child nodes
		self.n = 0			# Current no. of keys
		self.leaf = _leaf  # true when node is leaf

	# Traverse all subtrees rooted with this node
	global nodes
	def traverse(self):
		i=0
		while i<self.n:
			if self.leaf == False:
				self.C[i].traverse()
			nodes.append(self.keys[i])
			i+=1
		if self.leaf == False:
			self.C[i].traverse()
		return nodes

	def search(self,k):
		i = 0
		for key in self.keys:
			if k==key[1]:
				return key
		while i<self.n and k>self.keys[i][1]:
		 	i+=1
		if self.leaf == True:
			return None
		return self.C[i].search(k)

	def insertNonFull(self,k):
		i = self.n-1
		if self.leaf == True:
			if self.keys[i][1]>k[1]:
				self.keys.append(self.keys[i])
				while i>=0 and self.keys[i][1]>k[1]:
					self.keys[i+1]=self.keys[i]
					i-=1
				self.keys[i+1]=k
			else:
				self.keys.append(k)
			self.n = self.n+1
		else:
			while i>=0 and self.keys[i][1]>k[1]:
				i-=1
			if self.C[i+1].n==(2*self.t - 1):
				self.splitChild(i+1,self.C[i+1])
				if self.keys[i+1][1]<k[1]:
					i+=1
			self.C[i+1].insertNonFull(k)

	def splitChild(self,i,y):
		z = Node(y.t,y.leaf)
		z.n = self.t - 1
		for j in range(self.t-1):
			z.keys.append(y.keys[j+self.t])

		if y.leaf == False:
			for j in range(self.t):
				z.C[j] = y.C[j+self.t]

		y.n = self.t-1
		j = self.n
		while j>=i+1:
			if j==self.n:
				self.C.append(self.C[j])
			else:
				self.C[j+1]=self.C[j]
			j-=1
		self.C.append(z)

		j=self.n-1
		while j>=i:
			self.keys.append(self.keys[j])
			j-=1
		self.keys.append(y.keys[self.t-1])
		q=y.n
		while q<len(y.keys):
		    y.keys.pop(q)
		self.n = self.n + 1

class BTree():
	def __init__(self,_t):
		self.root = None
		self.t = _t

	def traverse(self):
		global nodes;
		if self.root!=None:
			test=self.root.traverse()
			nodes=[];
			return test;

	def search(self,k):
		if self.root==None:
			return None
		else:
			return self.root.search(k)

	def insert(self,k):
		if self.root == None:
			self.root = Node(self.t,True)
			self.root.keys.append(k)
			self.root.n = 1
		else:
			if self.root.n == (2*self.t - 1):
				s = Node(self.t,False)
				s.C.append(self.root)
				s.splitChild(0,self.root)
				i=0
				if s.keys[0][1]<k[1]:
					i+=1
				s.C[i].insertNonFull(k)
				self.root = s
			else:
				self.root.insertNonFull(k)
