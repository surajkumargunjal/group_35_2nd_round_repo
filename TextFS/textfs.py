import os
import sys
import random
from btree import BTree

class TextFS():
	def __init__(self):
		if os.path.exists('textfs.fs'):
			self.fs = open("textfs.fs","rb+")
		else:
			# Create FS and set default Attributes (Superblock)
			self.fs = open("textfs.fs","wb+")
			self.fs.write("4096\n")
			self.fs.write("32\n")
			self.fs.write("511\n")
			self.fs.write("1\n")
			self.fs.write("12288\n")
			
			self.fillBlock(4095)
			
			# Create Inode Lists 
			self.fs.write(";".join(str(x) for x in range(1,512)))
			self.fs.write("\n")
			self.fs.write("0:sample.txt")

			ptr = self.fs.tell()
			self.fillBlock(12287)
			
			# Create Memory Blocks
			for i in range(1024):
				for j in range(4095):
					self.fs.write(" ")
				self.fs.write("\n")

			self.fs.seek(12288)
			
			# create a sample file
			self.fs.write("0\n")
			self.fs.write("sample.txt\n")
			ptr = self.fs.tell()
			self.fillBlock(16382)
			
			self.fs.write("Hello world! This is a sample file!!\t")
			ptr = self.fs.tell()
			self.fillBlock(20478)
			
		# Get Superblock Infromation
		self.fs.seek(0)

		superblock = self.fs.read(4095)
		superblock = superblock.split("\n")
		
		self.blockSize = int(superblock[0])
		self.blockGroup = int(superblock[1])
		self.freeInodes = int(superblock[2])
		self.usedInodes = int(superblock[3])
		self.dataBlockStartAddr = int(superblock[4])
		self.treeObject = BTree(3)
		
		# Traverse to data block start address
		self.getInodeTable()
		self.fs.seek(self.dataBlockStartAddr)
		#print self.fs.tell()

	# Fill Block with white spaces
	def fillBlock(self,n):
		for i in range(n-self.fs.tell()):
			self.fs.write(" ")
		self.fs.write("\n")

	# Function To get a tuple of inode and file name from usedInodeList	
	def splitter(self,x):
		x = x.split(":")
		return (int(x[0]),x[1])	

	# Get Inode tables information
	def getInodeTable(self):	
		self.fs.seek(4096)
		inodeList = self.fs.read(8191)
		inodeList = inodeList.split("\n")
		#print inodeList
		self.freeInodeList,self.usedInodeList = inodeList[0],inodeList[1]
		self.usedInodeList = self.usedInodeList.replace(" ","")
		
		self.freeInodeList = map(int,self.freeInodeList.split(";"))
		if len(self.usedInodeList)>0:
			self.usedInodeList = map(self.splitter,self.usedInodeList.split(";"))

			del self.treeObject
			self.treeObject = BTree(3)
			for inode in self.usedInodeList:
				self.treeObject.insert(inode)
		else:
			self.usedInodeList = []	
		#print self.freeInodeList
		#print self.usedInodeList

	# Update Inode Table after an operation	
	def updateInodeTable(self,freeList,usedList):
		freeList = ";".join(str(x) for x in freeList)
		usedList = ";".join(str(x[0])+":"+str(x[1]) for x in usedList)
		self.fs.seek(4096)
		self.fs.write(freeList)
		self.fs.write("\n")
		self.fs.write(usedList)
		self.fillBlock(12287)
		self.getInodeTable()

	# Find A File in TextFS	
	def find(self,fileName):
		addr = self.dataBlockStartAddr
		Address = -1

		ans = self.treeObject.search(fileName)
		if ans!=None:
			#print "using B tree"
			offset = ans[0] * 8192
			self.fs.seek(addr+offset)
			meta = self.fs.read(4095)
			Address = self.fs.tell()
			#print Address
		return Address			

	# Change Free and Used inodes in superblock
	def updateSuperblock(self):
		self.fs.seek(8)
		self.fs.write(str(len(self.freeInodeList))+"\n")
		self.fs.write(str(len(self.usedInodeList))+"\n")	

	# Create a new File in TextFS	
	def create(self,fileName):
		ret_addr = self.find(fileName)
		if ret_addr == -1:	
			inode = random.choice(self.freeInodeList)
			#print inode
			offset = inode * 8192
			self.fs.seek(self.dataBlockStartAddr+offset)
			self.fs.write(str(inode)+"\n")
			self.fs.write(fileName+"\n")
			ptr = self.fs.tell()
			self.fillBlock(4094)
			print "File "+fileName+" Created Successfully!"

			#modify inode list
			self.freeInodeList.pop(self.freeInodeList.index(inode))
			self.usedInodeList.append((inode,fileName))
			
			self.updateSuperblock()
			self.updateInodeTable(self.freeInodeList,self.usedInodeList)
		else:
			print "File Already Exists!!\nUse Another Filename!!\n"	

	# Print The Content of File		
	def echo(self,fileName):
		content_addr = self.find(fileName)
		if content_addr!= -1:
			self.fs.seek(content_addr)
			content = self.fs.read(4095).split("\t")
			if len(content)>1: 		
				print "File Found!\nContent:\n",content[0]
			else:
				print "File is Empty!\nUse 'copy' command to copy content of other file!\n"
		else:
			print "File Not Found!"

	# Copy Content of External file to a File		
	def copy(self,file1,file2):
		if os.path.isfile(file1):	
			infile = open(file1,"rb")
			content = infile.read()
			infile.close()
			outfile_addr = self.find(file2)
			if outfile_addr != -1:
				self.fs.seek(outfile_addr)
				self.fs.write(content+"\t")
				print "Content Copied Successfully!\n"
			else:
				print "File Doesn't exist!\nCreate File using 'create' command!\n"
		else:
			print "Input External File Does not Exist!!\nGive a correct path!\n"

	# List all Files in TextFS
	def list(self):
		addr = self.dataBlockStartAddr
		for inode in self.usedInodeList:
			offset = inode[0] * 8192
			self.fs.seek(addr+offset)
			meta = self.fs.read(4095)
			meta = meta.replace(" ","")
			meta = meta.split("\n")
			print "%-15s" %meta[1],
		print ""

	# Delete Existing file
	def delete(self,fileName):
		content_addr = self.find(fileName)
		if content_addr!= -1:
			self.fs.seek(content_addr-4095)
			meta = self.fs.read(4095)
			meta = meta.replace(" ","")
			meta = meta.split("\n")
			#print meta
			inode= int(meta[0])
			
			self.fs.seek(content_addr-4095)
			self.fillBlock(self.fs.tell()+4094)
			self.fillBlock(self.fs.tell()+4094)

			self.usedInodeList.pop([x[0] for x  in self.usedInodeList].index(inode))
			self.freeInodeList.append(inode)
			self.updateInodeTable(self.freeInodeList,self.usedInodeList)
			self.updateSuperblock()

			print "File Deleted Successfully!!\n"
		else:
			print "File Does not exist!!\nPlease enter name of existing file!!\n"

if __name__=="__main__":
	textfs = TextFS()
	commands = ["create","echo","ls","copy","delete","exit","help"]
	while True:
		cmd = raw_input("$ ")
		cmd = cmd.split()
		if cmd[0] not in commands:
			print "Enter a valid command!\nType help for commands"
			continue
		elif cmd[0]=="echo":
			if len(cmd)==2:
				textfs.echo(cmd[1])
			else:
				print "Usage $ echo file_name\n"
		elif cmd[0]=="create":
			if len(cmd)==2:
				textfs.create(cmd[1])
			else:
				print "Usage $ create file_name\n"
		elif cmd[0]=="copy":
			if len(cmd)==3:
				textfs.copy(cmd[1],cmd[2])
			else:
				print "Usage $ copy externa_file_name TextFS_file_name\n"
		elif cmd[0]=="ls":
			if len(cmd)==1:
				textfs.list()
			else:
				print "Usage $ ls\n"
		elif cmd[0]=="help":
			if len(cmd)==1:
				print "create\t\tCreate a File\necho\t\tRead a File\nls\t\tList all Files\ncopy\t\tCopy content of external file to TextFS file\nexit\t\tQuit\n"
			else:
				print "Usage $ help\n"
		elif cmd[0]=="delete":
			if len(cmd)==2:
				textfs.delete(cmd[1])
			else:
				print "Usage $ delete file_name\n"
		elif cmd[0]=="exit":
			print "Bye"
			break
