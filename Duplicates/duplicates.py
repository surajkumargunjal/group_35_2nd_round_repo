import os, sys, hashlib

class DuplicateManager():
	def __init__(self):
		self.duplicates = {}

	# Get the md5 value of File Content
	def getHash(self,filename):
		f = open(filename,"rb")
		h = hashlib.md5()
		content = f.read()
		h.update(content)
		f.close()
		return h.hexdigest()

	# Traverse Recursively through given directory
	# Find Duplicate files using md5 values
	def findDuplicates(self,parent):
		for directory, subirectories, files in os.walk(parent):
			for file in files:
				file_path = os.path.join(directory,file)
				md5_val = self.getHash(file_path)
				if md5_val in self.duplicates.keys():
					self.duplicates[md5_val].append(file_path)
				else:
					self.duplicates[md5_val] = [file_path]

	# Prints the output as lists containing path of duplicate files
	def printResults(self):
		for key in self.duplicates.keys():
			print self.duplicates.keys().index(key)+1,") ",self.duplicates[key]

	# Check if duplicate files are available in given directory
	def duplicateAvailable(self):
		count = 0
		for value in self.duplicates.values():
			if len(value)==1:
				count += 1
		if count == len(self.duplicates.values()):
			return False
		else:
			return True

	# Remove all duplicate files
	def removeDuplicates(self):
		for dupFiles in self.duplicates.values():
			i=1
			while i<len(dupFiles):
				os.system("rm "+dupFiles[i])
				i+=1
		print "Successfully Deleted Duplicate Files!!!"
		
if __name__=="__main__":
	d = DuplicateManager()
	if len(sys.argv)==2:
		d.findDuplicates(sys.argv[1])
		if d.duplicateAvailable():
			d.printResults()
			while True:
				ch = raw_input("Do you want to remove duplicate files? (Y/N)\n")
				if ch=="Y" or ch=="y":
					d.removeDuplicates()
					break
				elif ch=="N" or ch=="n":
					break
				else:
					print "Enter choice as 'Y/y' or 'N/n'!!"
		else:
			print "No Duplicates Available!!"
	else:
		print "Usage python duplicates.py folderName"