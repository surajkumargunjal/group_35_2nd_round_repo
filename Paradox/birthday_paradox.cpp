#include<bits/stdc++.h>
using namespace std;

#define SIZE 5

int main()
{
	int count=0;
	int people[SIZE]={23,34,57,70,365};
	for(int k=0;k<SIZE;k++)
	{	
		count=0;
		for(int i=0;i<1000;i++)
		{
			list<int> l;
			set<int> s;
			srand(i);
			for(int j=0;j<people[k];j++)
			{
				int birthday=rand()%365+1;
				l.push_back(birthday);
				s.insert(birthday);
			}
			if(s.size()!=l.size()) 
				count++;	
		}
		printf("%d --> %5.3f\n",people[k],((float)count/1000.000000)*100);
	}	
	return 0;
}
