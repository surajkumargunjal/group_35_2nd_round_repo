import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class SVC {
	//store program path

    //store program path
	public static String PROGRAM_PATH;

	public SVC() throws IOException {
		SVC.PROGRAM_PATH = new File(".").getCanonicalPath() + "/";
	}

	//store file as it is in 0th version
    //exact copy of file for 0th version
	public void initializeVersion(String name) throws IOException {
		FileWriter fileWriter = new FileWriter(SVC.PROGRAM_PATH + "0.svc");

		Scanner scanner = new Scanner(new FileReader(name));

		int row = 0;
		while (scanner.hasNext()) {
			fileWriter.write(row + " " + scanner.nextLine() + "\n");
			row++;
		}
		scanner.close();
		fileWriter.close();

	}

	//build file upto nth version
    //apply changes one by one from .svc version files 
	public ArrayList<String> buildPrevVersion(int version, boolean print)
			throws FileNotFoundException {

		ArrayList<String> file = new ArrayList<String>();
		int j = 0;

		Scanner scanner = null;
		int lineno;
		String line;

		for (int i = 0; i < version; i++) {

			scanner = new Scanner(new FileReader(SVC.PROGRAM_PATH + i + ".svc"));
			j = 0;

			while (scanner.hasNext()) {
				lineno = scanner.nextInt();
				line = scanner.nextLine();

				if (!line.equals(" "))
					file.add(line.substring(1));
				else {
					file.remove(lineno - j);
					j++;
				}
			}
		}

		if (print)
			for (String s : file)
				System.out.println(s);

		scanner.close();
		return file;
	}

	//build current version and save to file
    //save .svc file for current version
	public void buildCurrentVersion(String filename, int version)
			throws IOException {
		ArrayList<String> file = buildPrevVersion(version, false);

		Scanner scanner = new Scanner(new FileReader(SVC.PROGRAM_PATH
				+ filename));
		FileWriter fileWriter = new FileWriter(SVC.PROGRAM_PATH + version
				+ ".svc");

		int row1 = 0, row2 = 0;
		String line = null;
		boolean inc = true;

		while (scanner.hasNext()) {
			if (row2 >= file.size())
				break;

			if (inc)
				line = scanner.nextLine();

			//System.out.println(file.get(row2) + " ----------" + line);
			while (row2 < file.size() && !line.equals(file.get(row2))) {
				fileWriter.write(row2 + " " + "\n");
				row2++;
				inc = false;
				// continue;
			}
			row1++;
			row2++;
			inc = true;
		}

		while (row2 < file.size()) {
			fileWriter.write(row2 + " " + "\n");
			row2++;
		}

		while (scanner.hasNext()) {
			line = scanner.nextLine();
			fileWriter.write(row1 + " " + line + "\n");
			row1++;
		}
		fileWriter.close();
		scanner.close();
	}

	//get files with .svc extension
    //return array of all svc files from current directory
	public File[] fileFinder(String dirname) {
		File dir = new File(dirname);
		File[] files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".svc");
			}
		});
		return files;
	}

	public static void main(String[] args) throws IOException {
		SVC svc = new SVC();

		if(args.length!=1){
			System.out.println("USAGE:\njava SVC filename\njava SVC VERSION_NO");
			return;
		}
		
		final String FILE_NAME = args[0];
		int VERSION = 0;


		//check whether to display or commit
        //decide whether to build version or display previous version
		boolean commmit_or_view = true;

		try {
			VERSION = Integer.parseInt(FILE_NAME);
			commmit_or_view = false;

		} catch (NumberFormatException ex) {
			File[] files = svc.fileFinder(SVC.PROGRAM_PATH);

			if (files.length == 0) {
				svc.initializeVersion(FILE_NAME);
				System.out.println("Version "+ 0 + " Commited.");
				return;
			}

			ArrayList<File> filesarr = new ArrayList<File>(Arrays.asList(files));

			Collections.sort(filesarr);

			VERSION = Integer.parseInt(filesarr.get(files.length - 1).getName()
					.replaceAll(".svc", ""));

		}catch(ArrayIndexOutOfBoundsException ex){
			System.out.println("USAGE:\njava SVC filename\njava SVC VERSION_NO");
		} catch (Exception e) {
			e.printStackTrace();
		}

		VERSION++;
		try {

			if (commmit_or_view) {
				svc.buildCurrentVersion(FILE_NAME, VERSION);
				System.out.println("Version "+VERSION + " Commited.");
			} else {
				svc.buildPrevVersion(VERSION, true);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
