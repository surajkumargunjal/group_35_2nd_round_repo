## 1. Birthday Paradox Problem

Introduction
--------------

The birthday paradox, also known as the birthday problem, states that in a random group of 23 people, there is about a 50 percent chance that two people have the same birthday. Is this really true?
We verify this problem by giving an input of randomly generated birthdays for 23 people(1000 such set)from the 365 days in an year and checking the probability of two people having the same birthday.
We also check probabilities against a varied number of people.

The output is the number of people vs the probability of two people having the same birthday in that group.


Operating Instructions
------------------------

To run the C++ code use:

        $ g++ birthday_paradox.cpp

        $ ./a.out

## 2. Simple Version Control

Introduction
--------------

Version control is a system that records changes to a file or set of files over time so that you can recall specific versions later. Our program 'svc' does version control on text files.


Operating Instructions
------------------------

To run the code use:

        $ javac SVC.java

After compilation create a text file whose changes have to be tracked and then use

        $ java SVC filename.txt

This command allows to commit changes to filename.txt

One can then add or delete lines from text file.

        $ java SVC n

where n starts from 0.

Here n indicates version of the textfile. Incremental versions are obtained only after commiting changes

This command allows u to view output of the nth version of the file.

## 3. Duplicate Files Identifier

Introduction
--------------

The program lists all the duplicate files from the hard drive and give user option to remove them or merge them.

Operating Instructions
------------------------

To run the code:

        $ python duplicates.py /path/where/duplicates/to/be/checked

EXAMPLE : $ python duplicates.py /home/User/Test

The duplicates files(if any) are listed. The duplicate files are identified by multiple values in the list. If there is more than one file in the list it indicates that the files in the list are duplicates of each other. The lists having only single files are files which are not duplicated elsewhere in the folder path.

EXAMPLE : 

        $ python duplicates.py /home/User/Test

1 )  ['/home/User/Test/def.txt']

2 )  ['/home/User/Test/B/abc.txt', '/home/User/Test/A/hello.txt']

List '2)' is a list of duplicate files. List '1)' shows a unique file

An option of removing the duplicate files and merging them into a single file(the first file in the list of duplicate files) is given to the user. On selecting 'Y' to this option only the first file in the list is kept and others are deleted or we can say they are merged into the first files.

If there are no duplicates then a message 'No Duplicates Available!!' is diplayed.


Note: Temporary backup files(the ones ending with ~) maybe displayed as duplicate files if they contain same contents as a normal file.

## 4. TextFS

Introduction
--------------

Textfs that stores important files without revealing much information about them. Textfs is a text based file system ie. all the meta-data and the actual data of the files will go in a single text file. 

We have used BTree to store the used inodes and search a required file effectively

Operating Instructions
------------------------

To run the code use the following commands:

        $ python textfs.py

The textfs interpreter will launch.


This application works with the following commands :
     Command		Use
----------------------------------------------------------------
1. help	: Show a list of all available commands

        usage : $ help

2. create : Create a new file

        usage : $ create test.txt

3. copy	: Copy content of external files into these newly created files
   
        usage : $ copy /external/path/to/file.txt destfile.txt

4. echo	: Print contents of internal files

        usage : $ echo test.txt

5. delete : Delete existing file

        usage : $ delete test.txt

6. ls : List all files

        usage : $ ls

7. exit	: exit the TextFS interpreter
